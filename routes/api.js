var express = require('express');
var router = express.Router();
var apiController = require('../controllers/apiController');



// USER ROUTES

//  USER CREATE
router.post('/user/create', apiController.postUserCreate); 

// // POST USER UPDATE
router.post('/user/:user_id/update', apiController.postUserUpdate); 

// // GET USER LIST
router.get('/users', apiController.getUserList); 

// // GET USER DETAILS
router.get('/user/:user_id', apiController.getUserDetails);

// delete user
router.get('/user/:user_id/delete', apiController.getUserDelete); 




module.exports = router;