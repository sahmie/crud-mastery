var models = require('../models');

// POST USER CREATE

exports.postUserCreate = async function(req, res, next) {
    
try {
            if(!req.body.email || !req.body.password){
               return res.status(404).json({status: false, message: "Email or Password is required"})
           }
           
           
            // check for user in db
           const current_business = await models.CurrentBusiness.findOne({ where: { current_business_name: req.body.current_business } });
         
           //check for role in db
            const role = await models.Role.findOne({ where: { role_name: req.body.role } });
    
           //check for department in db
            const department = await models.Department.findOne({ department_name: {  name: req.body.department_name } });
        
           //check for profile in db
          
            const profile = await models.Profile.findOne({ where: { profile_name: req.body.profile } });
            
             if( (!current_business) || (!role) || (!department) || (!profile)){
              return res.status(404).json({ status: false, message: "please select a (business || role || profile || department ) that's in the database",  });
          }
           
  
       var user = await models.User.create({
            first_name: req.body.first_name,
            last_name: req.body.last_name,
            email: req.body.email,
            username: req.body.username,
            DepartmentId: department.id,
            ProfileId: profile.id,
            RoleId: role.id,
            CurrentBusinessId: current_business.id,
        })

        console.log("The user id " + user.id);
        
        
        console.log('User Created Successfully');
    
        return res.status(200).json({
            status: true,
            message: "User created successfully",
            data: user,
        });
        
    } catch (error) {
        // we have an error during the process, then catch it and redirect to error page
        console.log("There was an error " + error);
        // remove the user that was created so we don't have a user without permission.
        models.User.destroy({ where: {id: user.id}});
        // now render error page
        res.status(503).json({ status: false, message: "An error occured while creating user", data: error })
    }
};




// UPDATE USER
exports.postUserUpdate = async function(req, res, next) {
    
    try {
        
          if(!req.body.email || !req.body.password){
               return res.status(404).json({status: false, message: "Email or Password is required"})
           }
           
           
              // check for user in db
           const current_business = await models.CurrentBusiness.findOne({ where: { current_business_name: req.body.current_business } });
         
           //check for role in db
            const role = await models.Role.findOne({ where: { role_name: req.body.role } });
    
           //check for department in db
            const department = await models.Department.findOne({ department_name: {  name: req.body.department_name } });
        
           //check for profile in db
          
            const profile = await models.Profile.findOne({ where: { profile_name: req.body.profile } });
            
             if( (!current_business) || (!role) || (!department) || (!profile)){
              return res.status(404).json({ status: false, message: "please select a (business || role || profile || department ) that's in the database",  });
          }
        
        await models.User.update(
            // Values to update
            {
                first_name: req.body.first_name,
                last_name: req.body.last_name,
                email: req.body.email,
                username: req.body.username,
                DepartmentId: req.body.department_id,
                ProfileId: req.body.profile_id,
                RoleId: req.body.role_id,
                CurrentBusinessId: req.body.current_business_id
            }, { // Clause
                where: {
                    id: req.params.user_id
                }
            }
        );
        
        
        var user = await models.User.findByPk(req.params.user_id);
        
        
        return res.status(200).json({
            status: true,
            message: "User updated successfully",
            data: user,
        });
    
    
    } catch (error) {
        // we have an error during the process, then catch it and redirect to error page
        console.log("There was an error " + error);
       res.status(503).json({ status: false, message: "An error occured while updating user", data: error });
    }
};




// GET ALL USERS
exports.getUserList = async function(req, res, next) {

  try {
      const users = await models.User.findAll();
      
      if(users.length == 0){
          return res.status(404).json({ status: false, message: "No user found", data: users  });
      } else {
          return res.status(200).json({ status: true, message: "User list rendered successfully", data: users,  });
      }
       
        
    
  } catch (error) {
        console.log("There was an error " + error);
       res.status(503).json({ status: false, message: "An error occured while updating user", data: error });
  }
};


// GET USER DETAIL
exports.getUserDetails = async function(req, res, next) {


  try {
       const user = await models.User.findByPk(
        req.params.user_id, {
            include:
            [
                        {
                            model: models.Department 
                        },
                        {
                            model: models.Role
                        },
                        {
                            model: models.Profile
                        },
                        {   model: models.Post},
                        {   model: models.CurrentBusiness},
                        {
                            model: models.Permission,
                            as: 'permissions',
                            attributes: ['id', 'permission_name']
                        } 
                        
            ]
        }
    );
    
    if(!user){
          return res.status(404).json({ status: false, message: "No user found", data: user });
      } else {
          return res.status(200).json({ status: true, message: "User found", data: user,  });
      }
    
  } catch (error) {
        console.log("There was an error " + error);
       res.status(503).json({ status: false, message: "An error occured while updating user", data: error });
  }
};


// Display User delete form on GET.
exports.getUserDelete = async function(req, res, next) {
  try {
      
      const user = await models.User.findByPk(req.params.user_id);
      
     if(!user){
          return res.status(404).json({ status: false, message: "No user found", data: user });
      } else {
      await models.User.destroy({
          where: {
            id: user.id
        }
      })
         return res.status(201).json({ status: true, message: "User deleted successfully", data: user,  });
          
      }
    
  } catch (error) {
       console.log("There was an error " + error);
       res.status(503).json({ status: false, message: "An error occured while deleting user", data: error });
  }
};








async function CreateOrUpdatePermissions(req, res, user, actionType) {

    let permissionList = req.body.permissions;
    
    console.log(permissionList);
    
    console.log('type of permission list is ' + typeof permissionList);
    
    // I am checking if permissionList exist
    if (permissionList) { 
        
        // I am checking if only 1 permission has been selected
        // if only one permission then use the simple case scenario for adding permission
        if(permissionList.length === 1) {
            
        // check if we have that permission that was selected in our database model for permission
        const permission = await models.Permission.findByPk(permissionList);
        
        console.log("These are the permissions " + permission);
        
        // check if permission exists
        if (!permission) {
            // destroy the user we created - since the permission selected does not exist.
            // we don't want to have a user in db without a permission
            // note this is only for actionType create i.e. when we are creating a new user
            if(actionType == 'create') models.User.destroy({ where: {id: user.id}});
            return res.status(422).json({ status: false,  error: 'Cannot find that permission selected'});
        }
        
        // since we want to update the permission, lets remove the previous permissions for that user.
        //  remove association before update new entry inside UserPermission table if it exist
        if(actionType == 'update') {
            const oldPermissions = await user.getPermissions();
            await user.removePermissions(oldPermissions);
        }
        
        // now, create a new permission for the user.
        await user.addPermission(permission);
        return true;
    }
    
    // Ok now lets do for more than 1 permissions, the hard bit.
    // if more than one permissions have been selected
    else {
        
        // object might not be the perfect typeof to check - this is just. quick way for me.
        if(typeof permissionList === 'object') {
            // Loop through all the ids in req.body.permissions i.e. the selected permissions
            await permissionList.forEach(async (id) => {
                // check if all permission selected are in the database
                const permission = await models.Permission.findByPk(id);
                
                if (!permission) {
                    // return res.status(400);
                    // destroy the user we created, we don't want user without permission in db
                    if(actionType == 'create') models.User.destroy({ where: {id: user.id}});
                    return res.status(422).json({ status: false,  error: 'Cannot find that permission selected'});
                }
                
                // remove previous associations before we update
                if(actionType == 'update') {
                    const oldPermissions = await user.getPermissions();
                    await user.removePermissions(oldPermissions);
                }
                 
                // now add new permission after removal
                await user.addPermission(permission);
                
            });
            
            return true;
            
        } else {
            // destroy the user we created since permission type is not object
            if(actionType == 'create') models.User.destroy({ where: {id: user.id}});
            return res.status(422).json({ status: false,  error: 'Type of permission list is not an object'});
        }
    }} else {
            // destroy user we created since no permission was selected.
            if(actionType == 'create') { models.User.destroy({ where: {id: user.id}});}
            return res.status(422).json({ status: false,  error: 'No permission selected'});
        }
    
}
